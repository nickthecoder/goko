package uk.co.nickthecoder.goko

import javafx.application.Application

/**
 * The entry point into the application.
 * This isn't in GoKoApp because Java doesn't start up correctly when the class containing main is of
 * type Application, and the application does not use modules. Grr.
 */
class GoKo {
    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Application.launch(GoKoApp::class.java, *args)
        }
    }
}