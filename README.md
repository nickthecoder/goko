GoKo
====

GoKo is a cross-platform Go board.
You can play Go against the computer (using GNU's go AI), play a local two player game,
review and edit games and hone your go skills by solving go problems.
Go problems, and a Joseki dictionary can be downloaded from _Preferences_ section of GoKo.

It includes some features not found in a typical Go program,
e.g. game variations, such as _Hidden Move Go_, and _One Colour Go_.

![screenshot](screenshot.jpeg)

Status
------

Goko was supposed to be a one-stop-shop for all Go related stuff,
including playing on-line against other humans.
But I never got around to writing that part.

Also, when I wrote this, Alpha-Go didn't exist. At some point I should replace the
crusty old GNU Go engine with a modern AI.
Alas, my interest in Go has waned, and Goko will probably never be completed.

Build from Source
-----------------

    cd goko
    ./gradle

Precompiled binaries
--------------------

[Download](http://nickthecoder.co.uk/software/goko/download/

You will need Java.

Run `goko/bin/goto`
